using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApplicationAPI.Data;
using ApplicationAPI.Model;

namespace ApplicationAPI.Controllers
{
    public class ScoresController : ApiController
    {
        private ApplicationAPIContext db = new ApplicationAPIContext();

        // GET: api/Scores
        public IQueryable<Score> GetScores()
        {
            return db.Scores;
        }

        // GET: api/Scores/5
        [ResponseType(typeof(Score))]
        public IHttpActionResult GetScore(int id)
        {
            Score score = db.Scores.Find(id);
            if (score == null)
            {
                return NotFound();
            }

            return Ok(score);
        }

        // GET: api/topExcuseDelays
        [HttpGet]
        [Route("api/topExcuseDelays")]
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelayTopScore()
        {
            var queryTopScore = from c in db.Scores
                                join p in db.ExcuseDelays
                                on c.ExcuseDelayId equals p.Id
                                group new { c, p } by new { c.ScoreLesson, p.Description, p.Titre, p.DelayTime, p.Id, p.Photo } into gr
                                orderby gr.Sum(c => c.c.ScoreLesson) descending
                                select new
                                {
                                    gr.Key.Id,
                                    Amount = gr.Sum(c => c.c.ScoreLesson),
                                    gr.Key.Titre,
                                    gr.Key.Description,
                                    gr.Key.DelayTime,
                                    gr.Key.Photo
                                };
            queryTopScore = queryTopScore.Take(10);

            if (queryTopScore == null)
            {
                return NotFound();
            }

            return Ok<IQueryable>(queryTopScore);
        }

        // GET: api/topExcuseDelays
        [HttpGet]
        [Route("api/topExcuseDelaysToday")]
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelayTopScoreToday()
        {
            var queryTopScore = from c in db.Scores
                                join p in db.ExcuseDelays
                                on c.ExcuseDelayId equals p.Id
                                where p.DateCreated.Day == DateTime.Now.Day
                                group new { c, p } by new { c.ScoreLesson, p.Description, p.Titre, p.DelayTime, p.Id, p.Photo } into gr
                                orderby gr.Sum(c => c.c.ScoreLesson) descending
                                select new
                                {
                                    gr.Key.Id,
                                    Amount = gr.Sum(c => c.c.ScoreLesson),
                                    gr.Key.Titre,
                                    gr.Key.Description,
                                    gr.Key.DelayTime,
                                    gr.Key.Photo
                                };
            queryTopScore = queryTopScore.Take(10);

            if (queryTopScore == null)
            {
                return NotFound();
            }

            return Ok<IQueryable>(queryTopScore);
        }

        // GET: api/topExcuseDelays
        [HttpGet]
        [Route("api/topExcuseDelaysMonth")]
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelayTopScoreMonth()
        {
            var queryTopScore = from c in db.Scores
                                join p in db.ExcuseDelays
                                on c.ExcuseDelayId equals p.Id
                                where p.DateCreated.Month == DateTime.Now.Month
                                group new { c, p } by new { c.ScoreLesson, p.Description, p.Titre, p.DelayTime, p.Id, p.Photo } into gr
                                orderby gr.Sum(c => c.c.ScoreLesson) descending
                                select new
                                {
                                    gr.Key.Id,
                                    Amount = gr.Sum(c => c.c.ScoreLesson),
                                    gr.Key.Titre,
                                    gr.Key.Description,
                                    gr.Key.DelayTime,
                                    gr.Key.Photo
                                };
            queryTopScore = queryTopScore.Take(10);

            if (queryTopScore == null)
            {
                return NotFound();
            }

            return Ok<IQueryable>(queryTopScore);
        }
        // GET: api/topExcuseDelays
        [HttpGet]
        [Route("api/topExcuseDelaysYear")]
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelayTopScoreYear()
        {
            var queryTopScore = from c in db.Scores
                                join p in db.ExcuseDelays
                                on c.ExcuseDelayId equals p.Id
                                where p.DateCreated.Year == DateTime.Now.Year
                                group new { c, p } by new { c.ScoreLesson, p.Description, p.Titre, p.DelayTime, p.Id, p.Photo } into gr
                                orderby gr.Sum(c => c.c.ScoreLesson) descending
                                select new
                                {
                                    gr.Key.Id,
                                    Amount = gr.Sum(c => c.c.ScoreLesson),
                                    gr.Key.Titre,
                                    gr.Key.Description,
                                    gr.Key.DelayTime,
                                    gr.Key.Photo
                                };
            queryTopScore = queryTopScore.Take(10);

            if (queryTopScore == null)
            {
                return NotFound();
            }

            return Ok<IQueryable>(queryTopScore);
        }

        // PUT: api/Scores/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutScore(int id, Score score)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != score.Id)
            {
                return BadRequest();
            }

            db.Entry(score).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScoreExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Scores
        [ResponseType(typeof(Score))]
        public IHttpActionResult PostScore(Score score)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Scores.Add(score);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = score.Id }, score);
        }

        [ResponseType(typeof(Score))]
        public IHttpActionResult GetScoresByStudent([FromUri]int studentId)
        {
            if (db.Scores.Any(e => e.StudentId == studentId))
            {
                var query = db.Scores.Where(s => s.StudentId == studentId);
                if (query == null)
                {
                    return NotFound();
                }
                return Ok<IQueryable>(query);
            }
            return NotFound();
        }

        // DELETE: api/Scores/5
        [ResponseType(typeof(Score))]
        public IHttpActionResult DeleteScore(int id)
        {
            Score score = db.Scores.Find(id);
            if (score == null)
            {
                return NotFound();
            }

            db.Scores.Remove(score);
            db.SaveChanges();

            return Ok(score);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ScoreExists(int id)
        {
            return db.Scores.Count(e => e.Id == id) > 0;
        }
    }
}