﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ApplicationAPI.Data;
using System.Threading.Tasks;
using ApplicationAPI.Model;

namespace ApplicationAPI.Controllers
{
    public class ExcuseDelaysController : ApiController
    {
        private ApplicationAPIContext db = new ApplicationAPIContext();

        // GET: api/ExcuseDelays
        public IQueryable<ExcuseDelay> GetExcuseDelays()
        {
            return db.ExcuseDelays;
        }

        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelaysByStudent([FromUri]int studentId)
        {
            if (db.ExcuseDelays.Any(e => e.StudentId == studentId))
            {
                var query = db.ExcuseDelays.Include("Student").Where(s => s.StudentId == studentId);
                if (query == null)
                {
                    return NotFound();
                }
                return Ok<IQueryable>(query);
            }
            return NotFound();
        }

        // GET: api/ExcuseDelays/5
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult GetExcuseDelay(int id)
        {
            ExcuseDelay excuseDelay = db.ExcuseDelays.Find(id);
            if (excuseDelay == null)
            {
                excuseDelay = db.ExcuseDelays.Include("Student").First(s => s.Id == id);
                if (excuseDelay == null)
                {
                    return NotFound();
                }
                return Ok(excuseDelay);
            }
            return NotFound();
        }

        // PUT: api/ExcuseDelays/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExcuseDelay(int id, ExcuseDelay excuseDelay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != excuseDelay.Id)
            {
                return BadRequest();
            }

            db.Entry(excuseDelay).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExcuseDelayExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ExcuseDelays
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult PostExcuseDelay(ExcuseDelay excuseDelay)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ExcuseDelays.Add(excuseDelay);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = excuseDelay.Id }, excuseDelay);
        }

        // DELETE: api/ExcuseDelays/5
        [ResponseType(typeof(ExcuseDelay))]
        public IHttpActionResult DeleteExcuseDelay(int id)
        {
            ExcuseDelay excuseDelay = db.ExcuseDelays.Find(id);
            if (excuseDelay == null)
            {
                return NotFound();
            }

            db.ExcuseDelays.Remove(excuseDelay);
            db.SaveChanges();

            return Ok(excuseDelay);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExcuseDelayExists(int id)
        {
            return db.ExcuseDelays.Count(e => e.Id == id) > 0;
        }
    }
}