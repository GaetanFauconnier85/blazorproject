﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApplicationAPI.Model
{
    public class Score
    {
        [Key]
        public int Id { get; set; }

        public int ExcuseDelayId { get; set; }

        public int StudentId { get; set; }

        public int ScoreLesson { get; set; }
    }
}