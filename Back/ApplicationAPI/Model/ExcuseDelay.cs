﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApplicationAPI.Model
{
    public class ExcuseDelay
    {
        [Key]
        public int Id { get; set; }
        public String Description { get; set; }

        public String Titre { get; set; }

        public String Photo { get; set; }

        public int DelayTime { get; set; }

        public Student Student { get; set; }

        public int StudentId { get; set; }

        public DateTime DateCreated { get; set; }
    }
}