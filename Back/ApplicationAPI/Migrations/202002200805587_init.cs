﻿namespace ApplicationAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExcuseDelays",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Titre = c.String(),
                        Photo = c.String(),
                        DelayTime = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pseudo = c.String(),
                        Mail = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Scores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ExcuseDelayId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        ScoreLesson = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExcuseDelays", "StudentId", "dbo.Students");
            DropIndex("dbo.ExcuseDelays", new[] { "StudentId" });
            DropTable("dbo.Scores");
            DropTable("dbo.Students");
            DropTable("dbo.ExcuseDelays");
        }
    }
}
