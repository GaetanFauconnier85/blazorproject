﻿using Blazor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;

namespace Blazor.Data
{
    public class ExcuseDelayService
    {
        public HttpClient _httpClient;

        public ExcuseDelayService(HttpClient client)
        {
            _httpClient = client;
        }
        public async Task<List<ExcuseDelay>> GetExcuseDelayAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/ExcuseDelays");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<ExcuseDelay>>(responseContent);
        }
        public async void PostExcuseDelay(ExcuseDelay excuseDelay)
        {
            var serialized = JsonConvert.SerializeObject(excuseDelay);
            var stringContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            await _httpClient.PostAsync("http://localhost:1235/api/ExcuseDelays", stringContent).ConfigureAwait(false);
        }
        public async Task<List<TopScore>> GetExcuseDelayTopScoreAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/topExcuseDelays");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<TopScore>>(responseContent);
        }
        public async Task<List<TopScore>> GetExcuseDelayTopScoreTodayAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/topExcuseDelaysToday");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<TopScore>>(responseContent);
        }
        public async Task<List<TopScore>> GetExcuseDelayTopScoreMonthAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/topExcuseDelaysMonth");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<TopScore>>(responseContent);
        }
        public async Task<List<TopScore>> GetExcuseDelayTopScoreYearAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/topExcuseDelaysYear");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<TopScore>>(responseContent);
        }

        public async Task<ExcuseDelay> GetExcuseDelayByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"http://localhost:1235/api/ExcuseDelays/{id}");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<ExcuseDelay>(responseContent);
        }
        public async Task<List<ExcuseDelay>> GetExcuseDelayByStudentIdAsync(int studentId)
        {
            var response = await _httpClient.GetAsync($"http://localhost:1235/api/ExcuseDelays?StudentId={studentId}");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<ExcuseDelay>>(responseContent);
        }
    }
}
