﻿using Blazor.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Blazor.Data
{
    public class StudentService
    {
        public HttpClient _httpClient;

        private string baseUrl = "http://localhost:1235";

        public StudentService(HttpClient client)
        {
            _httpClient = client;
        }
        public async Task<List<Student>> GetStudentsAsync()
        {
            var response = await _httpClient.GetAsync($"{baseUrl}/api/Students");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<Student>>(responseContent);
        }

        public async Task<Student> GetStudentByIdAsync(int id)
        {
            var response = await _httpClient.GetAsync($"{baseUrl}/api/Students/{id}");
            response.EnsureSuccessStatusCode();

           using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<Student>(responseContent);
        }
        public async Task<List<Student>> GetStudentLogin(string pseudo, string password)
        {
            password = ComputeSha256Hash(password);

            var response = await _httpClient.GetAsync($"{baseUrl}/api/login?pseudo={pseudo}&password={password}");
            response.EnsureSuccessStatusCode();

            

            string responseContent = await response.Content.ReadAsStringAsync();

            List<Student> student = JsonConvert.DeserializeObject<List<Student>>(responseContent);
            return await Task.FromResult(student);
        }

        public async Task<List<Student>> GetStudentsAuth(string pseudo, string mail)
        {
            var response = await _httpClient.GetAsync($"{baseUrl}/api/register?pseudo={pseudo}&mail={mail}");
            response.EnsureSuccessStatusCode();

            string responseContent = await response.Content.ReadAsStringAsync();
            List<Student> student = JsonConvert.DeserializeObject<List<Student>>(responseContent);
            return await Task.FromResult(student);
        }

        public async Task<bool> PostUserAsync(Student student)
        {
            student.Password = ComputeSha256Hash(student.Password); 

            HttpContent toPost = new StringContent(System.Text.Json.JsonSerializer.Serialize(student), Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{baseUrl}/api/Students", toPost);
            return response.IsSuccessStatusCode;
        }

        public string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
