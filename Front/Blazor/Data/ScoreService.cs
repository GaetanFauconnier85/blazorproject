﻿using Blazor.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Blazor.Data
{
    public class ScoreService
    {
        public HttpClient _httpClient;

        public ScoreService(HttpClient client)
        {
            _httpClient = client;
        }
        public async Task<List<Score>> GetScoreAsync()
        {
            var response = await _httpClient.GetAsync("http://localhost:1235/api/Students");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<Score>>(responseContent);
        }
        public async void PostScoreAsync(Score score)
        {
            var serialized = JsonConvert.SerializeObject(score);
            var stringContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            Console.WriteLine(stringContent.Headers);
            await _httpClient.PostAsync("http://localhost:1235/api/scores", stringContent).ConfigureAwait(false);
        }
        public async Task<List<Score>> GetScoreByStudentIdAndExcuseDelayIdAsync(int studentId)
        {
            var response = await _httpClient.GetAsync($"http://localhost:1235/api/Scores?StudentId={studentId}");
            response.EnsureSuccessStatusCode();

            using var responseContent = await response.Content.ReadAsStreamAsync();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<List<Score>>(responseContent);
        }
    }
}
