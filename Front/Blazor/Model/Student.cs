﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blazor.Model
{
    public class Student
    {
        [Key]
        public int Id { get; set; }

        public String Pseudo { get; set; }

        public String Mail { get; set; }

        public String Password { get; set; }
    }
}