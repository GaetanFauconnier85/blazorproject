﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor.Data
{
    public class TopScore
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public string Titre { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
        public int IdExcuse { get; set; }
    }
}
